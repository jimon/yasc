/*
Copyright (C) 2012 Dmitry Ivanov

	Permission is hereby granted, free of charge, to any person obtaining
	a copy of this software and associated documentation files (the
	"Software"), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish,
	distribute, sublicense, and/or sell copies of the Software, and to
	permit persons to whom the Software is furnished to do so, subject
	to the following conditions:

The above copyright notice and this permission notice shall be included
	in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
	CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
	TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// Yet Another String Class by Dmitry Ivanov aka jimon
// support UTF-8
//
// example and tests functions

#include <iostream>
#include <conio.h>

#include "yasc.h"

// ------------------------------------------------- example 1
// show basic string allocation
void example1()
{
	printf("---- example 1\n");

	// any string must be allocated on pool, yStringContainer contains string pool
	yStringContainer container;

	// reserve pool
	container.Reserve(64);

	// allocate string at pool with size 8 bytes
	yString playerName = container.Allocate(16);

	// write something to string
	int count = sprintf(playerName.GetRaw(), "testname");
	playerName.SetLength(count); // don't forget to write length

	// get string from C string literal (non memory alloc here)
	yString compare1 = "othername";
	yString compare2 = "testname";

	// compare strings
	bool isCorrect1 = (playerName == compare1);
	bool isCorrect2 = (playerName == compare2);

	printf("'%s' compare '%s' = %i\n", playerName.c_str(), compare1.c_str(), isCorrect1);
	printf("'%s' compare '%s' = %i\n", playerName.c_str(), compare2.c_str(), isCorrect2);

	printf("---- example 1 end\n");
}

// ------------------------------------------------- example 2
// show basic string concate
void example2()
{
	printf("---- example 2\n");

	yStringContainer container(64);
	yStringConcate concate(container.Allocate(32));

	yString hello = "Hello"; // let first part as string literal

	yString world = container.Allocate(6);
	memcpy(world.GetRaw(), "world", 6); // other part we get dynamic (think of this as simulation function)
	world.SetLength(5);
	
	// lets concate hello world
	concate.Add("%s, %s", hello.c_str(), world.c_str());
	concate.Add(" !");
	yString helloWorld = concate.BakeToString();
	printf("result 1 : '%s'\n", helloWorld.c_str());

	// lets make clone of hello world
	yString helloWorldClone = container.Clone(helloWorld);
	printf("result 2 : '%s'\n", helloWorldClone.c_str());

	// lets concate new string
	yString test = concate.Add("test, ").Add("test %i", 2).BakeToString();
	printf("result 3 : '%s'\n", test.c_str());

	// lets output now hello world, as you now concate works at buffer string, and after each BakeToString it return this buffer string
	printf("result 4 : '%s'\n", helloWorld.c_str());

	// so we figure out that concate override out helloWorld string, this right
	// lets check helloWorldClone
	printf("result 5 : '%s'\n", helloWorldClone.c_str());

	// as expected it works, so if you want store concate result, you should place in at any container
	// but if you work with result at place, and want to know when data changed, you can simple compare strings
	// old string data is invalid, but compare methods works, thanks to hash compare
	bool compare1 = (helloWorld == test);
	printf("result 6 : %i\n", compare1);

	// so compare return false, as expected, but as you already know, if you doesnt store result, data become invalid, so next compare also false
	bool compare2 = (helloWorld == helloWorldClone);
	printf("result 7 : %i\n", compare2);

	printf("---- example 2 end\n");
}

// ------------------------------------------------- example 3
// show multiply containers usage
void example3()
{
	printf("---- example 3\n");

	yStringContainer container1(64);
	yStringContainer container2(64);

	yString a = container1.Allocate(32);
	yString b = container2.Allocate(32);

	a.SetLength(sprintf(a.GetRaw(), "some test"));
	b.SetLength(sprintf(b.GetRaw(), "some test"));

	bool compare1 = (a == b); // standart compare

	yString c = a;

	bool compare2 = (a == c); // hash compare, ultrafast

	printf("result : %i %i\n", compare1, compare2);

	printf("---- example 3 end\n");
}

// ------------------------------------------------- main
int main()
{
	printf("yasc tests\n");

	example1();
	example2();
	example3();

	printf("yasc tests ended\n");
	printf("press any key...");

	_getch();
	return 0;
}
